module.exports = function(grunt) {
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            'dist/*',
            'dist/git*'
          ]
        }]
      },
      server: {
        files: [{
          dot: true,
          src: [
            '<%= pkg.serverPath %>/*',
            '<%= pkg.serverPath %>/git*'
          ]
        }]
      }      
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'src/{,*/}*.js',
        //'src/js/modules/sliderousel.activePage.js',
        'src/js/modules/{,*/}*.js'
      ]
    },
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: [
          'src/js/*.js',
          //'src/js/modules/sliderousel.activePage.js',
          'src/js/modules/{,*/}*.js'
        ],
        // the location of the resulting JS file
        dest: 'dist/js/<%= pkg.name %>.js'
      },
      server: {
        // the files to concatenate
        src: ['src/js/*.js'],
        // the location of the resulting JS file
        dest: '<%= pkg.serverPath %>/js/<%= pkg.name %>.js'
      }      
    },
    uglify: {
      options: {
        mangle: true
      },
      dist: {
        src: '<%= concat.dist.dest %>',
        dest: 'dist/js/<%= pkg.name %>.min.js'
      },
      server: {
        src: '<%= concat.dist.dest %>',
        dest: '<%= pkg.serverPath %>/js/<%= pkg.name %>.min.js'
      }
    },
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'src',
          dest: 'dist',
          src: [
            '*.{ico,png,txt,html}',
            '.htaccess',
            'images/{,*/}*.{webp,png,jpg,gif}'
          ]
        }]
      },
      server: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'src',
          dest: '<%= pkg.serverPath %>',
          src: [
            '*.{ico,png,txt,html}',
            '.htaccess',
            'images/{,*/}*.{webp,png,jpg,gif}'
          ]
        }]
      }      
    },
    docco: {
      src: ['src/js/*.js'],
      dest: 'docs'
    },
    watch: {
      scripts: {
        files: 'src/**/*',
        tasks: ['build']
      }
    }    
  });

  grunt.registerTask('build', [
    'clean:dist',
    'concat:dist',
    'uglify:dist',
    'copy:dist'
  ]);

  grunt.registerTask('push', [
    'clean:server',
    'concat:server',
    'uglify:server',
    'copy:server'
  ]);

  grunt.registerTask('default', [
    'jshint',
    'build'
  ]);
};