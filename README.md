
# csSliderousel

Customizable javascript carousel

## Options
---

##### timeOut

	Timeout to use between transition state
	Accepts: integer (in milliseconds)
	Default: 6000

##### speed
	Speed of timeout animation
	Accepts: integer (in milliseconds)
	Default: 600

#### autoPlay
	Autoplay carousel on page load
	Accepts: boolean
	Default: false
	Notes: sliderousel.timing.js module must be included for autoPlay functionality

#### circular
	Repeat carousel elements after last slide
	Accepts: boolean
	Default: false

#### slideCount
	The amount of carousel items to slide
	Accepts: integer
	Default: 3
	Note: slideCount must be less than or equal to upCount

#### upCount
	The amount of carousel items to show
	Accepts: integer
	Default: 3

#### itemMargin
	The margin to use between carousel elements
	Accepts: integer
	Default: 15

#### itemWidth
	How the carousel determines itemWidth
	Accepts: 'auto' or integer
	Default: 'auto'


## Advanced Options
---

#### animation
	The transition animation to use for overlays 
	Accepts: 'fade', 'slide'
	Default: 'slide'
												
#### easing
	The easing type to use for transitions, only for javascript animations
	Accepts: string
	Default: 'swing'
												
#### pager
	Show the carousel pager
	Accepts: boolean
	Default: true
												
#### nav
	Show the carousel navigation (arrows)
	Accepts: boolean
	Default: true

#### resumeAfterInteraction
	Resume autoplay after the user has interacted with the carousel
	Accepts: boolean
	Default: false
	Note: Not implemented

#### removeExcessSpacing
	On transition, remove any excess blank elements on the last slide
	Accepts: boolean
	Default: false
	Note: Not implemented
												
#### timer
	Show the carousel timer
	Accepts: boolean
	Default: false
												
#### circularMode
	Mode for circular wrapping
	Accepts: 'infinite', 'wrap'
	Default: 'wrap'
	Notes: Only wrap mode is implemented at this time

#### centerNav
	Auto-center the navigation elements
	Accepts: boolean
	Default: true
	Notes: To center nav arrows, .nav-container must be a child of .bullet-container

#### cssanimation
	Use CSS animation over javascript animation when available
	Accepts: boolean
	Default: true

#### cssTransition
	The CSS easing to use when CSS animation is enabled
	Accepts: string
	Default: cubic-bezier(0.39, 0.575, 0.565, 1)

#### responsive
	Enabled resizing on window size change
	Accepts: boolean
	Default: false

#### responsiveMode
	Technique of resizing to use when responsive mode is enabled
	Accepts: string
	Default: 'center'
	Note: only 'center' mode is implemented at this time

#### dragSlide:
	Enable touch sliding
	Accepts: boolean
	Default: false
	Note: must include sliderousel.touchEnabled.js to use this option

#### minItemWidth
	The minimum item width before reducing upCount in responsive mode
	Accepts: integer
	Default: 0


## Customizable HTML Elements

---

#### slideContainer
	The slideContainer element to use
	Accepts: string
	Default: '.slide-container'

#### slideWrapper
	The slideWrapper element to use
	Accepts: string
	Default: '.cs-slider-wrapper'

#### bulletContainer
	The bulletContainer element to use
	Accepts: string
	Default: '.bullet-container'

#### navContainer
	The navContainer element to use
	Accepts: string
	Default: '.nav-container'

#### sliderMask
	The sliderMask element to use
	Accepts: string
	Default: .slider-mask'

#### slideClass
	The slide element to use
	Accepts: string
	Default: '.slide'

### Events

#### onInitialized
	Callback for after the carousel is initialized
	Accepts: function
	Default: $.noop 

#### onPlay
	Callback for the play event
	Accepts: function
	Default: $.noop
												
#### onPause
	Callback for the pause event
	Accepts: function
	Default: $.noop
												
#### onBeforeSlidechange
	Callback before the slide has changed
	Accepts: function
	Default: $.noop
	Arguments: current slide, next slide

#### onBeforeAfterchange
	Callback after the slide has changed
	Accepts: function
	Default: $.noop
	Arguments: current slide, next slide


## Exposed Triggers
Use triggers to hook into certain events to extend sliderousel by external modules or custom interactions

	beforeInit
	afterInit
	beforeUpdate
	afterUpdate

## Exposed Events
Use triggers to call the following events

	play
	pause
	stop
	next
	prev


### Todo
	Have touchDelta keep track of horizontal and vertical points
	Test in IE9-10
	Fix last slider timer running twice

## Dependencies 
* jQuery 1.7

## Examples

[http://codepen.io/justindmyers/pen/FDHwe](http://codepen.io/justindmyers/pen/FDHwe)
